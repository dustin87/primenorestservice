package com.freeenergy.demo.service.jpa;

import com.freeenergy.demo.dao.RecordDao;
import com.freeenergy.demo.model.Record;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Service
public class RecordService
{
    @Autowired RecordDao recordDao;

    public List<Record> getAllRecords(){
        return recordDao.findAll();
    }

    public Optional<Record> getOne(long id){
        return recordDao.findById(id);
    }

    public Record save(Record record){
        return recordDao.save(record);
    }

    public void delete(Record record){
        recordDao.delete(record);
    }

    public Record saveRealtime(String algorithmChosen, Timestamp eventTimestamp, int noOfPrimes, String range, long timeelapsed)
    {
        Record record = new Record();
        record.setAlgorithmChosen(algorithmChosen);
        record.setEventTimestamp(eventTimestamp);
        record.setNoOfPrimes(noOfPrimes);
        record.setRange(range);
        record.setTimeElapsed(timeelapsed);
        return recordDao.save(record);
    }

}
