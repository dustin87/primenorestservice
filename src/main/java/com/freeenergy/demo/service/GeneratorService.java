package com.freeenergy.demo.service;

import com.freeenergy.demo.model.Record;
import com.freeenergy.demo.service.jpa.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.math.BigInteger;
import java.sql.Timestamp;

@Service
public class GeneratorService
{
    @Autowired RecordService recordService;

    public String generatePrimeNo1(int low, int high)
    {
        Timestamp startTime = new Timestamp(System.currentTimeMillis());

        //validation
        if(low >= high){
            recordService.saveRealtime("ALGORITHM 1", new Timestamp(System.currentTimeMillis()), 0,
                    "From "+low + " to "+ high, 0);

            return "The first number must be lower than the second number";
        }

        if(high == 1){
            recordService.saveRealtime("ALGORITHM 1", new Timestamp(System.currentTimeMillis()), 0,
                    "From "+low + " to "+ high, 0);

            return "No Prime numbers";
        }


        //generate prime numbers
        String primeNumbers = "";
        int primeNoCount = 0;
        for (int i = low; i <= high; i++ )
        {
            if(i == 0 || i == 1) {
                continue;
            }

            boolean isPrime2 = true;
            for(int num = 2; num < i; num++)
            {
                /*System.out.println("low: "+ i + ", num: "+ num + ", modulus is: "+ i % num);*/
                if(i % num == 0)
                {
                    isPrime2 = false;
                }
            }

            if(isPrime2)
            {
                primeNumbers += i + ", ";
                primeNoCount++;
            }
        }
        Timestamp endTime = new Timestamp(System.currentTimeMillis());




        //save record
        recordService.saveRealtime("ALGORITHM 1", new Timestamp(System.currentTimeMillis()), primeNoCount,
                "From "+low + " to "+ high, endTime.getTime() - startTime.getTime());

        if(primeNoCount == 0){
            return "NO PRIME NUMBERS FOUND WITHIN THE RANGE OF "+ low + " and "+ high;
        }else{
            return "PRIME NUMBERS FOUND WITHIN THE RANGE OF "+ low + " and "+ high +":  "+ primeNumbers;
        }


    }

    public String generatePrimeNo2(int low, int high)
    {
        Timestamp startTime = new Timestamp(System.currentTimeMillis());

        //validation
        if(low >= high){
            recordService.saveRealtime("ALGORITHM 2", new Timestamp(System.currentTimeMillis()), 0,
                    "From "+low + " to "+ high, 0);
            return "The first number must be lower than the second number";
        }

        if(high == 1){
            recordService.saveRealtime("ALGORITHM 2", new Timestamp(System.currentTimeMillis()), 0,
                    "From "+ low + " to " + high, 0);
            return "No Prime numbers";
        }


        //generate prime numbers
        String primeNumbers = "";
        int primeCount = 0;
        for (int i = low; i <= high; i++ )
        {
            if(i == 0 || i == 1) {
                continue;
            }



            boolean isPrime2 = true;
            for(int num = 2; num < i; num++)
            {
                double d = (double)i / (double)num;
                if((d - (int)d) == 0){
                    isPrime2 = false;
                }
            }

            if(isPrime2 == true) {
                primeNumbers = primeNumbers + i + ", ";
                primeCount++;
            }
        }
        Timestamp endTime = new Timestamp(System.currentTimeMillis());




        //save Record
        recordService.saveRealtime("ALGORITHM 2", new Timestamp(System.currentTimeMillis()), primeCount,
                "From "+ low + " to " + high, endTime.getTime() - startTime.getTime());


        if(primeCount == 0){
            return "NO PRIME NUMBERS FOUND WITHIN THE RANGE OF "+ low + " and "+ high;
        }else{
            return "PRIME NUMBERS FOUND WITHIN THE RANGE OF "+ low + " and "+ high +":  "+ primeNumbers;
        }

    }

    public String generatePrimeNo3(int low, int high)
    {
        Timestamp startTime = new Timestamp(System.currentTimeMillis());

        //validation
        if(low >= high){
            recordService.saveRealtime("ALGORITHM 3", new Timestamp(System.currentTimeMillis()), 0,
                    "From "+low + " to "+ high, 0);

            return "The first number must be lower than the second number";
        }

        if(high == 1){
            recordService.saveRealtime("ALGORITHM 3", new Timestamp(System.currentTimeMillis()), 0,
                    "From "+low + " to "+ high, 0);

            return "No Prime numbers";
        }


        //generate prime numbers
        StringBuilder st = new StringBuilder();
        int primeCount = 0;
        for (int i = low; i <= high; i++ )
        {
            if(i == 0 || i == 1) {
                continue;
            }


            //second check
            boolean isPrime2 = true;
            for(int num = 2; num < i; num++) {
                if(i % num == 0)
                {
                    isPrime2 = false;
                }
            }

            if(isPrime2) {
                st.append(i);
                st.append(", ");
                primeCount++;
            }
        }

        Timestamp endTime = new Timestamp(System.currentTimeMillis());


        //save Record
        recordService.saveRealtime("ALGORITHM 3", new Timestamp(System.currentTimeMillis()), primeCount,
                "From "+ low + " to " + high, endTime.getTime() - startTime.getTime());


        if(primeCount == 0){
            return "NO PRIME NUMBERS FOUND WITHIN THE RANGE OF "+ low + " and "+ high;
        }else{
            return "PRIME NUMBERS FOUND WITHIN THE RANGE OF "+ low + " and "+ high +":  "+ st;
        }

    }
}
