package com.freeenergy.demo.dao;

import com.freeenergy.demo.model.Record;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RecordDao extends JpaRepository<Record, Long> {
}
