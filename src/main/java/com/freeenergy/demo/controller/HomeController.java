package com.freeenergy.demo.controller;

import com.freeenergy.demo.model.dto.ResponseObject;
import com.freeenergy.demo.service.GeneratorService;
import com.freeenergy.demo.service.jpa.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;

@RestController
public class HomeController
{
    @Autowired GeneratorService generatorService;
    @Autowired RecordService recordService;


    /**
     * Description - To generate prime numbers which fall within the specified range
     * @param methodNo - to specify one of the three algorithms for generating the prime numbers
     * @param firstNo - lowest threshold of the range
     * @param secondNo - highest threshold of the range
     *
     */
    @GetMapping("/prime_numbers/method/{methodNo}/range/{firstNo}/{secondNo}")
    public ResponseEntity<?> getPrimeNo(@PathVariable("methodNo") int method,
                                        @PathVariable("firstNo") int no1, @PathVariable("secondNo") int no2)
    {
        //Validation
        if(method < 1 || method > 3){
            ResponseObject obj = new ResponseObject();
            obj.setStatus(ResponseObject.Status.FAILURE);
            obj.setContent("You need to make a method selection between 1 and 3 only");

            return new ResponseEntity<>(obj, HttpStatus.BAD_REQUEST);
        }

        if(no1 >= no2){
            ResponseObject obj = new ResponseObject();
            obj.setStatus(ResponseObject.Status.FAILURE);
            obj.setContent("The first number must be lower than the second number");

            return new ResponseEntity<>(obj, HttpStatus.BAD_REQUEST);
        }

        if(no2 == 1){
            ResponseObject obj = new ResponseObject();
            obj.setStatus(ResponseObject.Status.FAILURE);
            obj.setContent("No Prime numbers");

            return new ResponseEntity<>(obj, HttpStatus.BAD_REQUEST);
        }


        //Generate Prime numberS
        String response = "";
        if(method == 1){
            response = generatorService.generatePrimeNo1(no1, no2);
        }
        else if(method == 2){
            response = generatorService.generatePrimeNo2(no1, no2);
        }
        else if(method == 3){
            response = generatorService.generatePrimeNo3(no1, no2);
        }


        //send response
        ResponseObject obj = new ResponseObject();
        obj.setStatus(ResponseObject.Status.SUCCESS);
        obj.setContent(response);

        return new ResponseEntity<>(obj, HttpStatus.OK);
    }


    /**
     * Description - To display ALL records stored on the DB
     * @return
     */
    @GetMapping("/prime_numbers/get/all")
    public ResponseEntity<?> getPrimeNo()
    {


        //send response
        ResponseObject obj = new ResponseObject();
        obj.setStatus(ResponseObject.Status.SUCCESS);
        obj.setObject((Serializable) recordService.getAllRecords());

        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
}
