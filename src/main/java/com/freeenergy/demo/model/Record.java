package com.freeenergy.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity
public class Record {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private Timestamp eventTimestamp;
    private String range;
    private long timeElapsed;
    private String algorithmChosen;
    private long noOfPrimes;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Timestamp getEventTimestamp() {
        return eventTimestamp;
    }

    public void setEventTimestamp(Timestamp eventTimestamp) {
        this.eventTimestamp = eventTimestamp;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public long getTimeElapsed() {
        return timeElapsed;
    }

    public void setTimeElapsed(long timeElapsed) {
        this.timeElapsed = timeElapsed;
    }

    public String getAlgorithmChosen() {
        return algorithmChosen;
    }

    public void setAlgorithmChosen(String algorithmChosen) {
        this.algorithmChosen = algorithmChosen;
    }

    public long getNoOfPrimes() {
        return noOfPrimes;
    }

    public void setNoOfPrimes(long noOfPrimes) {
        this.noOfPrimes = noOfPrimes;
    }
}
