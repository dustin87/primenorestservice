package com.freeenergy.demo.model.dto;

import java.io.Serializable;

public class ResponseObject {
    public enum Status{SUCCESS, FAILURE, PENDING};

    public Status status;
    public String content;
    public Serializable object;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Serializable getObject() {
        return object;
    }

    public void setObject(Serializable object) {
        this.object = object;
    }
}
